<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $title ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>/user_panel/vendors/feather/feather.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>/user_panel/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>/user_panel/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>/user_panel/css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="icon" href="<?php echo base_url() ?>img/sifatan.png" type="image/x-icon">
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <center><img src="<?php echo base_url() ?>/img/sifatan.png" alt="logo"></center>
              </div>
              <h4>Silakan Login disini</h4>
			  <br/>
				<?php echo $this->session->flashdata('notif') ?>
				<?php echo form_open_multipart('login/aksi_login') ?>
              <form class="pt-3">
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Username Anda" name="user" autofocus>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password Anda" name="sandi">
                </div>
                <div class="mt-3">
				  <button type="submit" class="btn btn-block btn-success btn-lg font-weight-medium auth-form-btn">LOGIN</button>
                </div>
				<?php echo form_close() ?>
              </form>		  
            </div>	
			<div class="text-center">
			&copy; 
			<?php
				$fromYear = 2022; 
				$thisYear = (int)date('Y'); 
				echo $fromYear . (($fromYear != $thisYear) ? ' - ' . $thisYear : '');
			?>
			<?php echo $Footer ?>
			</div>			
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
	
  </div>
  
	
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?php echo base_url() ?>/user_panel/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="<?php echo base_url() ?>/user_panel/js/off-canvas.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/template.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/settings.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/todolist.js"></script>
  <!-- endinject -->
</body>
</html>