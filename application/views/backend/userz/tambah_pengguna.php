<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="page-wrapper">
	<div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 card">			
				<div class="card-body">
				<?php echo form_open_multipart('pengguna/insert') ?>
					<div class="col-md-6" style="float: left;">				
						<div class="form-group">
							<label for="text">ID Cart</label>
							<input type="text" name="Txtidpengguna" class="form-control" placeholder="Masukkan ID Cart" required autofocus>
						</div>
						<div class="form-group">
							<label for="text">Nama Karyawan</label>
							<input type="text" name="Txtnamapengguna" class="form-control" placeholder="Masukkan Nama Karyawan" required autofocus>
						</div>
						<div class="form-group">
							<label for="text">No. HP</label>
							<input type="text" name="Txthp" class="form-control" placeholder="Masukkan No HP" required>
						</div>
					</div>
					<div class="col-md-6" style="float: left;">
						<div class="form-group" required>
							<label for="text">Level Karyawan</label>
							<select class="js-example-basic-single w-100" name="CmbLevel" required>
								<option disabled selected>--Pilih--</option>
								<option value="operasional">Operasional</option>
								<option value="kasir">Kasir</option>
								<option value="akuntansi">Akuntansi</option>
							</select>
						</div>
						<div class="form-group">
							<label for="text">Username</label>
							<input type="text" name="username" class="form-control" placeholder="Masukkan Username Pengguna" required>
						</div>
						<div class="form-group">
							<label for="text">Password</label>
							<input type="password" name="pass" class="form-control" placeholder="Masukkan Password Pengguna" required>
						</div>
					</div>
				</div>
				<div class="border-top card-body">	
					<button type="submit" class="btn btn-md btn-success">Simpan</button>
					<button type="reset" class="btn btn-md btn-warning">reset</button>
					<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>    
				</div>
            </div>
        </div>
    </div>
</div>