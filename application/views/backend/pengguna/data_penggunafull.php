<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
        <!-- [ Main Content ] start -->
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<h5>Tambah Data User</h5>
						</div>
						<div class="card-body">
						<div class="alert alert-mafan alert-dismissible" role="alert"><b><?php echo $this->session->flashdata('notif') ?>Data Hanya Bisa diinput 1 Kali, Kesalahan Data adalah Tanggung Jawab Penuh User Aplikasi.</b></div>
							<?php echo form_open_multipart('pengguna/insert') ?>
							<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="text">OPD</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<select class="js-example-basic-single w-100"  name="Txtnik">
										<option disabled selected>--Pilih--</option>
										<?php
										foreach ($jns_nik as $Dtnik) {
											?>
											<option value="<?= $Dtnik->nik ?>"><?= $Dtnik->nik ?> <?= $Dtnik->nama_pemilih ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label for="text">Unit Kerja</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<select class="js-example-basic-single w-100"  name="Txtnik">
										<option disabled selected>--Pilih--</option>
										<?php
										foreach ($jns_nik as $Dtnik) {
											?>
											<option value="<?= $Dtnik->nik ?>"><?= $Dtnik->nik ?> <?= $Dtnik->nama_pemilih ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label for="text">Username</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="text" name="username" class="form-control" placeholder="Masukkan Username Pengguna" required>
								</div>
								<div class="form-group">
									<label for="text">Password</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="password" name="pass" class="form-control form-password" placeholder="Masukkan Password Pengguna" value="<?= set_value('pass'); ?>" required>
									<input type="checkbox" class="form-checkbox"> Show password
								</div>
										
							</div>
							</div>
							<button type="submit" class="btn btn-md btn-warning">Simpan</button>
							<?php echo form_close() ?>
						</div>
					</div>
				
				</div>
				<!-- [ form-element ] start -->
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<h5>Tambah Data User</h5>
						</div>
						<div class="card-body">
							<table id="example" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
								<thead>
									<tr>
										<th>No.</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Username</th>
										<th>Kecamatan</th>
										<th>Kelurahan/Desa</th>
										<th>Kontak</th>
										<th>level</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($data_allpengguna as $hasil) {
										$xidu=$hasil->nik;
									?>
									<tr>
										<td style="width: 8%;"><?php echo $no++ ?></td>
										<td><?php echo $hasil->id_pengguna ?></td>
										<td><?php echo $hasil->nama_pemilih ?></td>
										<td><?php echo $hasil->username ?></td>
										<td><?php echo $hasil->nama_kecamatan ?></td>
										<td><?php echo $hasil->nama_keldes ?></td>
										<td><?php echo $hasil->kontak ?></td>
										<td><?php echo $hasil->level ?></td>
										<td style="width: 20%;">
										<a href="<?php echo base_url() ?>pengguna/edit/<?php echo $xidu ?>" class="btn btn-sm btn-success">Edit</a>
										<a href="<?php echo base_url() ?>pengguna/hapus/<?php echo $hasil->nik ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus ?')">Hapus</a>
										</td>
									</tr>
									<?php } ?>												
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</section>