<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	if ($this->session->userdata('level') == "superadmin") {
?>
<!-- partial:../../partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() ?>dashboard">
              <i class="icon-grid menu-icon"></i>
              <span class="menu-title">Home</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-opd" aria-expanded="false" aria-controls="form-opd">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">OPD</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-opd">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>opdentry">Tambah Surat</a></li>

                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>opdview">Surat Masuk</a></li>

              </ul>
            </div>
          </li>
		  <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-protokoler" aria-expanded="false" aria-controls="form-protokoler">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">Protokoler</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-protokoler">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <!--<a class="nav-link" href="<?php echo base_url() ?>protokolerentry">Tambah Surat</a></li>-->

                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>protokolerview">Protokoler Fase 1</a></li>
				
				<li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>protokolerreview">Protokoler Fase 2</a></li>
              </ul>
            </div>
          </li>
		  </li>
		  <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-disposisi" aria-expanded="false" aria-controls="form-disposisi">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">Disposisi</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-disposisi">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>disposisiview">Surat Masuk</a></li>

              </ul>
            </div>
          </li>
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">User Pages</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/login.html"> Login </a></li>
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/register.html"> Register </a></li>
              </ul>
            </div>
          </li>-->

        </ul>
      </nav>
      <!-- partial -->
<?php
	}elseif($this->session->userdata('level') == "opd") {
?>
	<!-- partial:../../partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() ?>dashboard">
              <i class="icon-grid menu-icon"></i>
              <span class="menu-title">Home</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-opd" aria-expanded="false" aria-controls="form-opd">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">Persuratan</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-opd">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>opdentry">Tambah Surat</a></li>

                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>opdview">Surat Masuk</a></li>

              </ul>
            </div>
          </li>
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">User Pages</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/login.html"> Login </a></li>
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/register.html"> Register </a></li>
              </ul>
            </div>
          </li>-->

        </ul>
      </nav>
      <!-- partial -->
<?php
	}elseif($this->session->userdata('level') == "protokol") {
?>
	<!-- partial:../../partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() ?>dashboard">
              <i class="icon-grid menu-icon"></i>
              <span class="menu-title">Home</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-opd" aria-expanded="false" aria-controls="form-opd">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">OPD</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-opd">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>opdentry">Tambah Surat</a></li>

                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>opdview">Surat Masuk</a></li>

              </ul>
            </div>
          </li>
		  <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-protokoler" aria-expanded="false" aria-controls="form-protokoler">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">Protokoler</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-protokoler">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <!--<a class="nav-link" href="<?php echo base_url() ?>protokolerentry">Tambah Surat</a></li>-->

                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>protokolerview">Protokoler Fase 1</a></li>
				
				<li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>protokolerreview">Protokoler Fase 2</a></li>
              </ul>
            </div>
          </li>
		  </li>
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">User Pages</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/login.html"> Login </a></li>
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/register.html"> Register </a></li>
              </ul>
            </div>
          </li>-->

        </ul>
      </nav>
      <!-- partial -->
<?php
	}elseif($this->session->userdata('level') == "pejabat") {
?>
	<!-- partial:../../partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() ?>dashboard">
              <i class="icon-grid menu-icon"></i>
              <span class="menu-title">Home</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-disposisi" aria-expanded="false" aria-controls="form-disposisi">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">Disposisi</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-disposisi">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>disposisiview">Surat Masuk</a></li>

              </ul>
            </div>
          </li>
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">User Pages</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/login.html"> Login </a></li>
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/register.html"> Register </a></li>
              </ul>
            </div>
          </li>-->

        </ul>
      </nav>
      <!-- partial -->	  
<?php
	}
?>