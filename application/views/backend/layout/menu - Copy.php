<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	if ($this->session->userdata('level') == "superadmin") {
?>
<!-- partial:../../partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() ?>dashboard">
              <i class="icon-grid menu-icon"></i>
              <span class="menu-title">Home</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">Surat OPD</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-elements">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>opdentry">Tambah Surat</a></li>

                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() ?>opdview">Tampil Surat</a></li>

              </ul>
            </div>
          </li>

          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">User Pages</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/login.html"> Login </a></li>
                <li class="nav-item"> <a class="nav-link" href="../../pages/samples/register.html"> Register </a></li>
              </ul>
            </div>
          </li>-->

        </ul>
      </nav>
      <!-- partial -->
<?php
	}elseif($this->session->userdata('level') == "operasional") {
?>
<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar menu-light ">
		<div class="navbar-wrapper  ">
			<div class="navbar-content scroll-div " >
				<div class="main-menu-header">
					<img class="img-radius" src="<?php echo base_url() ?>assets/images/avatar.png" alt="User-Profile-Image">
				</div>
				<ul class="nav pcoded-inner-navbar ">
					<li class="nav-item pcoded-menu-caption">
					    <label>Master</label>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>dashboard" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>notification" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Notifikasi</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>dptfile" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Forms DPT</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>pendukungfile" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Form Pendukung</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>userfile" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Form User</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>suarafile" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Forms Suara</span></a>
					</li>
					
					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Perhitungan</span></a>
					    <ul class="pcoded-submenu">
					        <li><a href="<?php echo base_url() ?>totalsuara">Total Suara</a></li>
							<li><a href="<?php echo base_url() ?>realcount">Data Real Count</a></li>
					    </ul>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>report_realcount" class="nav-link "><span class="pcoded-micon"><i class="fa fa-print m-r-5 m-l-5"></i></span><span class="pcoded-mtext">Rekap Desa</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>countkecamatan" class="nav-link "><span class="pcoded-micon"><i class="fa fa-print m-r-5 m-l-5"></i></span><span class="pcoded-mtext">Rekap Kecamatan</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>datadiri/<?php echo $this->session->userdata("id_pengguna"); ?>" class="nav-link "><span class="pcoded-micon"><i class="fa fa-users m-r-5 m-l-5"></i></span><span class="pcoded-mtext">Data Diri</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>log-out" class="nav-link "><span class="pcoded-micon"><i class="fa fa-power-off m-r-5 m-l-5"></i></span><span class="pcoded-mtext">Logout</span></a>
					</li>
				</ul>
				<div class="main-menu-header">
				<div class="row">
				<div class="col-md-6">
				<img class="img-radius" src="<?php echo base_url() ?>gambar/golkar.png" alt="User-Profile-Image">
				</div>
				<div class="col-md-6">
				<img class="img-radius" src="<?php echo base_url() ?>gambar/hanura.png" alt="User-Profile-Image">
				</div>
				</div>
				</div>
			</div>
		</div>
	</nav>
	<!-- [ navigation menu ] end -->
<?php 
	}elseif($this->session->userdata('level') == "desa") { 
?>
<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar menu-light ">
		<div class="navbar-wrapper  ">
			<div class="navbar-content scroll-div " >
				<div class="main-menu-header">
					<img class="img-radius" src="<?php echo base_url() ?>assets/images/avatar.png" alt="User-Profile-Image">
				</div>
				<ul class="nav pcoded-inner-navbar ">
					<li class="nav-item pcoded-menu-caption">
					    <label>Master</label>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>notification" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Notifikasi</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>suarafile" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Forms Suara</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>datadiri/<?php echo $this->session->userdata("id_pengguna"); ?>" class="nav-link "><span class="pcoded-micon"><i class="fa fa-users m-r-5 m-l-5"></i></span><span class="pcoded-mtext">Data Diri</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>log-out" class="nav-link "><span class="pcoded-micon"><i class="fa fa-power-off m-r-5 m-l-5"></i></span><span class="pcoded-mtext">Logout</span></a>
					</li>
				</ul>
				<div class="main-menu-header">
				<div class="row">
				<div class="col-md-6">
				<img class="img-radius" src="<?php echo base_url() ?>gambar/golkar.png" alt="User-Profile-Image">
				</div>
				<div class="col-md-6">
				<img class="img-radius" src="<?php echo base_url() ?>gambar/hanura.png" alt="User-Profile-Image">
				</div>
				</div>
				</div>
			</div>
		</div>
	</nav>
	<!-- [ navigation menu ] end -->
<?php 
	}elseif($this->session->userdata('level') == "tps") { 
?>
<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar menu-light ">
		<div class="navbar-wrapper  ">
			<div class="navbar-content scroll-div " >
				<div class="main-menu-header">
					<img class="img-radius" src="<?php echo base_url() ?>assets/images/avatar.png" alt="User-Profile-Image">
				</div>
				<ul class="nav pcoded-inner-navbar ">
					<li class="nav-item pcoded-menu-caption">
					    <label>Master</label>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>suarafile" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Forms Suara</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>datadiri/<?php echo $this->session->userdata("id_pengguna"); ?>" class="nav-link "><span class="pcoded-micon"><i class="fa fa-users m-r-5 m-l-5"></i></span><span class="pcoded-mtext">Data Diri</span></a>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>log-out" class="nav-link "><span class="pcoded-micon"><i class="fa fa-power-off m-r-5 m-l-5"></i></span><span class="pcoded-mtext">Logout</span></a>
					</li>
				</ul>
				<div class="main-menu-header">
				<div class="row">
				<div class="col-md-6">
				<img class="img-radius" src="<?php echo base_url() ?>gambar/golkar.png" alt="User-Profile-Image">
				</div>
				<div class="col-md-6">
				<img class="img-radius" src="<?php echo base_url() ?>gambar/hanura.png" alt="User-Profile-Image">
				</div>
				</div>
				</div>
			</div>
		</div>
	</nav>
	<!-- [ navigation menu ] end -->	
<?php 
	}elseif($this->session->userdata('level') == "pengawas") { 
?>
<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar menu-light " style="height:auto;">
		<div class="navbar-wrapper  ">
			<div class="navbar-content scroll-div " >
				<div class="main-menu-header">
					<img class="img-radius" src="<?php echo base_url() ?>assets/images/avatar.png" alt="User-Profile-Image">
				</div>
				<ul class="nav pcoded-inner-navbar ">
					<li class="nav-item pcoded-menu-caption">
					    <label>Master</label>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>dashboard" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
					</li>
					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Data Pendukung</span></a>
					    <ul class="pcoded-submenu">
					        <li><a href="<?php echo base_url() ?>datakecamatan">Forms Kecamatan</a></li>
							<li><a href="<?php echo base_url() ?>datakeldes">Forms Kelurahan/Desa</a></li>
							<li><a href="<?php echo base_url() ?>datatps">Forms TPS</a></li>
					    </ul>
					</li>
					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Induk Data</span></a>
					    <ul class="pcoded-submenu">
							<li><a href="<?php echo base_url() ?>datadpt">Forms DPT</a></li>
					        <li><a href="<?php echo base_url() ?>datapendukung">Form Pendukung</a></li>
							<li><a href="<?php echo base_url() ?>datauser">Form User</a></li>
							<li><a href="<?php echo base_url() ?>datasuara">Forms Suara</a></li>
					    </ul>
					</li>
					<li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Perhitungan</span></a>
					    <ul class="pcoded-submenu">
					        <li><a href="<?php echo base_url() ?>perhitungansuara">Total Suara</a></li>
							<li><a href="<?php echo base_url() ?>rekapsuara">Data Real Count</a></li>
					        <li><a href="<?php echo base_url() ?>rekapdapil">Data Rekap Dapil</a></li>
							<li><a href="<?php echo base_url() ?>rekapkeldes">Data Rekap Keldes</a></li>
					    </ul>
					</li>
					<li class="nav-item">
					    <a href="<?php echo base_url() ?>log-out" class="nav-link "><span class="pcoded-micon"><i class="fa fa-power-off m-r-5 m-l-5"></i></span><span class="pcoded-mtext">Logout</span></a>
					</li>
				</ul>
				<div class="main-menu-header">
				<div class="row">
				<div class="col-md-6">
				<img class="img-radius" src="<?php echo base_url() ?>gambar/golkar.png" alt="User-Profile-Image">
				</div>
				<div class="col-md-6">
				<img class="img-radius" src="<?php echo base_url() ?>gambar/hanura.png" alt="User-Profile-Image">
				</div>
				</div>
				</div>
			</div>
		</div>
	</nav>
<!-- [ navigation menu ] end -->	
<?php }?>	