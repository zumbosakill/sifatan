    </div>
<!-- page-body-wrapper ends -->
	<!-- partial:../../partials/_footer.html -->
<footer class="footer">
  <center><span class="text-muted text-center text-sm-left d-block d-sm-inline-block">
		&copy; 
		<?php
			$fromYear = 2022; 
			$thisYear = (int)date('Y'); 
			echo $fromYear . (($fromYear != $thisYear) ? ' - ' . $thisYear : '');
		?>
		SIPATAN
	</span></center>
</footer>
<!-- partial -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?php echo base_url() ?>/user_panel/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <script src="<?php echo base_url() ?>/user_panel/vendors/moment/moment.min.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/vendors/fullcalendar/fullcalendar.min.js"></script>
  <!-- Plugin js for this page -->
  <script src="<?php echo base_url() ?>/user_panel/vendors/typeahead.js/typeahead.bundle.min.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/vendors/select2/select2.min.js"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="<?php echo base_url() ?>/user_panel/js/off-canvas.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/template.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/settings.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url() ?>/user_panel/js/file-upload.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/typeahead.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/select2.js"></script>
  <!-- End custom js for this page-->

  <!-- plugins:js -->

  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="<?php echo base_url() ?>/user_panel/vendors/chart.js/Chart.min.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/dataTables.select.min.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/data-table.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/dashboard.js"></script>
  <script src="<?php echo base_url() ?>/user_panel/js/Chart.roundedBarCharts.js"></script>
  <!-- End custom js for this page-->
  
<script>
(function($) {
  'use strict';
  $(function() {
    if ($('#calendar').length) {
      $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,basicWeek,basicDay'
        },
        defaultDate: '2017-07-12',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [{
            title: 'All Day Event',
            start: '2017-07-08'
          },
          {
            title: 'Long Event',
            start: '2017-07-01',
            end: '2017-07-07'
          },
          {
            id: 999,
            title: 'Repeating Event',
            start: '2017-07-09T16:00:00'
          },
          {
            id: 999,
            title: 'Repeating Event',
            start: '2017-07-16T16:00:00'
          },
          {
            title: 'Conference',
            start: '2017-07-11',
            end: '2017-07-13'
          },
          {
            title: 'Meeting',
            start: '2017-07-12T10:30:00',
            end: '2017-07-12T12:30:00'
          },
          {
            title: 'Lunch',
            start: '2017-07-12T12:00:00'
          },
          {
            title: 'Meeting',
            start: '2017-07-12T14:30:00'
          },
          {
            title: 'Happy Hour',
            start: '2017-07-12T17:30:00'
          },
          {
            title: 'Dinner',
            start: '2017-07-12T20:00:00'
          },
          {
            title: 'Birthday Party',
            start: '2017-07-13T07:00:00'
          },
          {
            title: 'Click for Google',
            url: 'http://google.com/',
            start: '2017-07-28'
          }
        ]
      })
    }
  });
})(jQuery);
</script>
</body>

</html>
