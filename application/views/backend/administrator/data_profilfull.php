<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="page-wrapper">
	<div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">
                        <?php echo $this->session->flashdata('notif') ?>
                        <a href="<?php echo base_url() ?>login/tambah_p" class="btn btn-md btn-success"><i class="fa fa-plus fa-sm"></i> Tambah</a>
                        <hr>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Username</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $no = 1;
                                    foreach ($data_allprofil as $hasil) {
                                        ?>

                                        <tr>
                                            <td style="width: 8%;"><?php echo $no++ ?></td>

                                            <td><?= $hasil->username ?></td>
                                            <td style="width: 20%;">
                                                <a href="<?php echo base_url() ?>login/edit/<?php echo $hasil->id_user ?>" class="btn btn-sm btn-success">Edit</a>
                                                <?php
                                                    if (($hasil->level) != 'superadmin') {
                                                        ?>
                                                    <a href="<?php echo base_url() ?>login/hapus/<?php echo $hasil->id_user ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus <?php echo $hasil->username ?> ?')">Hapus</a>
                                                <?php } ?>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>