<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="main-panel">
	<div class="content-wrapper">
	  <div class="row">


		<div class="col-12 grid-margin stretch-card">
		  <div class="card">
			<div class="card-body">
			  <h4 class="card-title"><?php echo $title ?></h4>
			<?php echo $this->session->flashdata('notif') ?>
			<?php echo form_open_multipart('surat_opd/update') ?>
			<input type="hidden" name="TxtIDsuratopd" value="<?= $data_jb->id_surat_opd ?>">
			
			  <form class="forms-sample">			  
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Kode/Nomor Surat</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtkodesurat" required  value="<?= $data_jb->kode_surat ?>">
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Perihal</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtperihal" required  value="<?= $data_jb->perihal ?>">
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Asal Surat</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtasal" required  value="<?= $data_jb->asal_surat ?>">
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Unit Kerja</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtunit" required  value="<?= $data_jb->unit_kerja ?>">
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Tanggal</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="date" class="form-control" name="txttgl" required  value="<?= $data_jb->tgl_kegiatan ?>">
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Waktu</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="time" class="form-control" name="txtwaktu" required  value="<?= $data_jb->waktu ?>">
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Lokasi</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtlokasi" required  value="<?= $data_jb->lokasi ?>">
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Dikemukakan Kepada</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<select class="js-example-basic-single w-100" name="txtpejabat">
						<option disabled selected>--Pilih--</option>
						<?php
						  foreach ($jns_pejabat as $Dtpejabat) {
						?>
						<option value="<?= $Dtpejabat->id_pejabat ?>" <?php if (($data_jb->pejabat) == ($Dtpejabat->id_pejabat)) {	echo 'selected'; } ?> >
							<?= $Dtpejabat->jabatan; ?>
						</option>
						
						<?php
						  }
						?>
					</select>				
				  </div>
				</div>
				<!--<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Note</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<textarea class="form-control" rows="4" name="txtnote"><?= $data_jb->note_opd ?></textarea>
				  </div>
				</div>-->
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Daftar Undangan</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="file" name="cover" class="file-upload-default" accept=".doc, .docx, .pdf">
				  <div class="input-group col-xs-12">
					<input type="text" class="form-control file-upload-info" disabled placeholder="*Type File yang di upload dalam bentuk PDF dan Ms. Word">
					<span class="input-group-append">
					  <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
					</span>
				  </div>
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Lampiran Surat</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="file" name="catalog" class="file-upload-default" accept=".doc, .docx, .pdf">
				  <div class="input-group col-xs-12">
					<input type="text" class="form-control file-upload-info" disabled placeholder="*Type File yang di upload dalam bentuk PDF dan Ms. Word">
					<span class="input-group-append">
					  <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
					</span>
				  </div>
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Lampiran Sambutan</label>
				  <div class="col-sm-9">
				    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="file" name="draft" class="file-upload-default" accept=".doc, .docx, .pdf">
				  <div class="input-group col-xs-12">
					<input type="text" class="form-control file-upload-info" disabled placeholder="*Type File yang di upload dalam bentuk PDF dan Ms. Word">
					<span class="input-group-append">
					  <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
					</span>
				  </div>
				  </div>
				</div>

				<hr><button type="submit" class="btn btn-primary mr-2">Submit</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>
			  </form>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<!-- content-wrapper ends -->
</div>