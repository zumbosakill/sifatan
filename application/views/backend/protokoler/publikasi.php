<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">


            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $title ?></h4>
				  <?php echo $this->session->flashdata('notif') ?>
                  <div class="row">
                    <div class="col-12">
					  <div class="table-responsive">
                        <table id="order-listing" class="table">
                          <thead>
                            <tr class="bg-primary text-white">
                                <th>No</th>
								<th>Lembar Pengantar Naskah</th>
								<th>Asal Surat</th>
								<th>Unit Kerja</th>
								<th>Dikemukakan Kepada</th>
								<th>Status</th>
								<th>Actions</th>
                            </tr>
                          </thead>
                          <tbody>
						  <?php
							$no = 1;
							foreach ($data_protokoler as $hasil) {
						  ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
								<td><table width="100%" class="table table-hover">
								<tbody>
								  <tr>
								    <td>Kode Surat</td>
								    <td>:</td>
								    <td><?php echo $hasil->kode_surat ?></td>
							      </tr>
								  <tr>
								    <td>Perihal</td>
								    <td>:</td>
								    <td><?php echo $hasil->perihal ?></td>
							      </tr>
								  <tr>
								    <td>Tanggal Surat</td>
								    <td>:</td>
								    <td><?php echo $hasil->tgl_kegiatan ?></td>
							      </tr>
								  <tr>
								    <td>Waktu Pelaksanaan</td>
								    <td>:</td>
								    <td><?php echo $hasil->waktu ?></td>
							      </tr>
								  <tr>
								    <td>Lokasi</td>
								    <td>:</td>
								    <td><?php echo $hasil->lokasi ?></td>
							      </tr>
								  <tr>
								    <td>Review Surat</td>
								    <td>:</td>
								    <td>
									<form action="" method="post"><button name="view" class="btn btn-secondary">View</button>
									</form>
									</td>
							      </tr>
								  </tbody>
								</table>
								</td>
								<td><?php echo $hasil->asal_surat ?></td>
								<td><?php echo $hasil->unit_kerja ?></td>
								<td>
									<?php foreach ($jns_pejabat as $stat) {
									if (($hasil->pejabat) == ($stat->id_pejabat)) {
									 echo $stat->jabatan; }
									}?>
								</td>
                                <td>
                                  <?php 
								  if($hasil->status=='0'){
									  $status="Di Proses";
									  echo "<label class='badge badge-outline-warning btn-fw'>$status</label>";
								  }elseif($hasil->status=='1'){
									$status="Di Teruskan";
									$query=$this->db->query("SELECT * FROM protokoler");
									$row = $query->row();
									foreach ($jns_pejabat as $stat) {
									if (($row->tujuan) == ($stat->id_pejabat)) {
									echo "<label class='badge badge-outline-info btn-fw'>$status <br>Kepada $stat->jabatan</label>";
									}} 
								  }else if($hasil->status=='2'){
									$status="Disposisi";
									$query=$this->db->query("SELECT * FROM disposisi");
									$row = $query->row();
									foreach ($jns_pejabat as $stat) {
									if (($row->disposisikan) == ($stat->id_pejabat)) {
									echo "<label class='badge badge-outline-info btn-fw'>$status <br>Kepada $stat->jabatan</label>";
									}} 
								  }else if($hasil->status=='3'){
									  $status="Reschedule";
									  echo "<label class='badge badge-outline-warning btn-fw'>$status</label>";
								  }else if($hasil->status=='4'){
									  $status="Hadir";
									  echo "<label class='badge badge-success btn-fw'>$status</label>";
								  }else{
									  $status="Publish";
									  echo "<label class='badge badge-outline-warning btn-fw'>$status</label>";
								  }
								   ?>
                                </td>
								<?php 
								$sql = $this->db->query("SELECT status FROM surat_opd where id_surat_opd='$hasil->id_surat_opd'");
								$cek_ed = $sql->num_rows();
								if ($cek_ed == 4) { 
								?>
								<td>								
									<label class="badge badge-info">Sudah di Proses</label>
								</td>
								<?php }else{ ?>
								<td>								
									<a href="<?php echo base_url() ?>ubahdata/protokoler/<?php echo $hasil->id_surat_opd ?>" class="badge badge-info">Proses</a>
								</td>
								<?php } ?>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>					
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->