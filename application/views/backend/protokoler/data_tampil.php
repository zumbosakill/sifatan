<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">


            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $title ?></h4>
				  <?php echo $this->session->flashdata('notif') ?>
                  <div class="row">
                    <div class="col-12">
					  <div class="table-responsive">
                        <table id="order-listing" class="table">
                          <thead>
                            <tr class="bg-primary text-white">
                                <th>No</th>
								<th>Tanggal</th>
								<th>Perihal</th>
								<th>Dikemukakan Kepada</th>
								<?php
								foreach ($data_protokoler as $hasil) {								
								$sql = $this->db->query("SELECT id_surat_opd FROM protokoler where id_surat_opd='$hasil->id_surat_opd'");
								$cek_ed = $sql->num_rows();
								if ($cek_ed > 0) { 
								?>
								<th>Disposisi</th>
								<?php }else{ ?>
								<th>Actions</th>
								<?php }} ?>
                            </tr>
                          </thead>
                          <tbody>
						  <?php
							$no = 1;
							foreach ($data_protokoler as $hasil) {
							
						  ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
								<td><?php echo $hasil->tgl_kegiatan ?></td>
								<td><?php echo $hasil->perihal ?></td>
                                <td>
									<?php foreach ($jns_pejabat as $stat) {
									if (($hasil->pejabat) == ($stat->id_pejabat)) {
									 echo $stat->jabatan; }
									}?>
								</td>
								<td>
                                  <?php 
								  if($hasil->status=='0'){
									$status=$hasil->keterangan;
								  ?>
								  <a href="<?php echo base_url() ?>ubahdata/protokoler/<?php echo $hasil->id_surat_opd ?>" class="badge badge-info">Proses</a>
								  <?php
								  }elseif($hasil->status=='1'){
									$status="DI TUJUKAN";
									$query=$this->db->query("SELECT * FROM protokoler");
									$row = $query->row();
									foreach ($jns_pejabat as $stat) {
									if (($row->tujuan) == ($stat->nip)) {
									echo "<label class='badge badge-success btn-fw'>$stat->jabatan</label>";
									}} 
								  }else if($hasil->status=='2'){
									$status="Disposisi";
									$query=$this->db->query("SELECT * FROM disposisi");
									$row = $query->row();
									foreach ($jns_pejabat as $stat) {
									if (($row->disposisikan) == ($stat->nip)) {
									echo "<label class='badge badge-success btn-fw'>$stat->jabatan</label>";
									}} 
								  }else if($hasil->status=='3'){
									  $status=$hasil->keterangan;
									  echo "<label class='badge badge-success btn-fw'>$status</label>";
								  }else{
									  $status=$hasil->keterangan;
									  echo "<label class='badge badge-success btn-fw'>$status</label>";
								  }
								   ?>
                                </td>
								
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>					
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->