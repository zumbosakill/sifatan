<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userz extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('model_userz');
		$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
    }

    function index()
    {
        $this->load->view('index');
    }

    function allpengguna()
    {
        $data = array(
            'title' => 'Data Pengguna',
            'data_allpengguna' => $this->model_userz->get_all(),
            'isi' => 'backend/pengguna/data_penggunafull'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    function tambah()
    {
        $data = array(
            'title' => 'Data Pengguna',
            'datapengguna' => $this->model_userz->get_all(),
            'isi' => 'backend/pengguna/tambah_pengguna'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    public function simpan()
    {
        $data = array(
			'id_pengguna' => htmlspecialchars($this->input->post('Txtidpengguna', true)),
			'nama' => htmlspecialchars($this->input->post('Txtnamapengguna', true)),
			'level' => htmlspecialchars($this->input->post('CmbLevel', true)),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => md5($this->input->post('pass'))
        );
        $this->model_userz->simpan($data);
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di simpan</div>');

        redirect('pengguna');
    }
	
	public function insert()
	{
		$data = array(
			'id_pengguna' => htmlspecialchars($this->input->post('Txtidpengguna', true)),
			'level' => htmlspecialchars($this->input->post('CmbLevel', true)),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => md5($this->input->post('pass'))
        );
		$this->db->insert('tbl_adm',$data);
		
		$data2 = array(
			'id_pengguna' => htmlspecialchars($this->input->post('Txtidpengguna', true)),
			'nama' => htmlspecialchars($this->input->post('Txtnamapengguna', true)),
			'telepon' => htmlspecialchars($this->input->post('Txthp', true))
        );
			$this->db->insert('tbl_user',$data2);

		$this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Pembelian berhasil di simpan</div>');
		redirect('pengguna');
	}
	
	public function edit($idp)
    {
        $data = array(
            'title' => 'Edit Profil',
            'data_p' => $this->model_userz->edit($idp),
			'data_x' => $this->model_userz->editx($idp),
            'isi' => 'backend/pengguna/edit_pengguna'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }
	
    public function update()
    {
        $id_u = $this->input->post("id");
        $password = $this->input->post("pass");
        $username = $this->input->post("username");
		$nama = $this->input->post("nama");
		
        $data = array(
            'username' => $this->input->post("username"),
            'password' => md5($password)
        );
		$data2 = array(
            'nama' => $this->input->post("nama"),
            'telepon' => $this->input->post("telp")
        );
		$this->model_userz->updatex($data, $id_u);
		$this->model_userz->update($data2, $id_u);
		
        $this->session->set_userdata('nama', $nama);
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Pengguna berhasil di Update</div>');

        redirect('pengguna');
    }
	
	public function hapus($id)
    {
        $idu['id_pengguna'] = $this->uri->segment(3);

        $this->model_userz->hapusu($idu);
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di hapus</div>');
        redirect('pengguna');
    }
}
