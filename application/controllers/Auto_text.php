<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auto_text extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Model_auto_text');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {
    $this->load->view('index');
  }
  
  public function tampil()
  {
    $data = array(
      'title' => 'Auto Text',
      'data_auto_text' => $this->Model_auto_text->get_all(),
      'isi' => 'backend/auto_text/data_tampil'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
	$data = array(
	  'title' => 'Input Surat',
	  'data_jb' => $this->Model_auto_text->get_all(),
	  'isi' => 'backend/auto_text/tambah_surat'
	);
	$this->form_validation->set_rules('txtisi', 'Isi Text', 'required');
		
		if ($this->form_validation->run() == false) {
            //GAGAL
            
			$this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
	
  }

  public function simpan()
  {	
	$data = array(
      'isi_text' => htmlspecialchars($this->input->post("txtisi", true))
    );
	
    $this->Model_auto_text->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Simpan</div>');

    redirect('textentry');
	}

  public function hapus($idj)
  {
    $id['id_auto_text'] = $this->uri->segment(3);
    $this->Model_auto_text->hapus($id);
    redirect('textview');
  }
  
  public function edit($idjb)
	{
        $idjb = $this->uri->segment(3);
		
		$data = array(
			  'title'     => 'Update Surat',
			  'data_jb' => $this->Model_auto_text->edit($idjb),
			  'isi' => 'backend/auto_text/perubahan_surat'
		);
		
		$this->form_validation->set_rules('txtisi', 'Isi Text', 'required');

        if ($this->form_validation->run() == false) {
            //GAGAL
            
			$this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->update();
        }
    }

  public function update()
  {
    $id['id_auto_text'] = $this->input->post("TxtIDtext");
	
    $data = array(
		'isi_text' => htmlspecialchars($this->input->post("txtisi", true))
        );

    $this->Model_auto_text->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Update</div>');

    redirect('textview');
  }

} // END OF class kecamatan
