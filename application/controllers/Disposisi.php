<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disposisi extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Model_disposisi');
	$this->load->model('Model_surat_opd');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {

    $this->load->view('index');
  }
  
  public function tampil()
  {

    $data = array(
      'title' => 'Agenda Acara',
      'data_disposisi' => $this->Model_disposisi->get_all(),
	  'data_protokoler' => $this->Model_disposisi->get_protokoler(),
	  'jns_pejabat' => $this->Model_disposisi->get_pejabat(),
      'isi' => 'backend/disposisi/data_tampil'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $this->form_validation->set_rules('txtkodesurat', 'Kode Surat', 'required');
	$this->form_validation->set_rules('txtperihal', 'Perihal', 'required');
	$this->form_validation->set_rules('txtasal', 'Asal', 'required');
	$this->form_validation->set_rules('txtunit', 'Unit', 'required');
	$this->form_validation->set_rules('txttgl', 'Tanggal', 'required');
	$this->form_validation->set_rules('txtwaktu', 'Waktu', 'required');
	$this->form_validation->set_rules('txtlokasi', 'Lokasi', 'required');
	$this->form_validation->set_rules('txtpejabat', 'Dikemukakan Kepada', 'required');
	$this->form_validation->set_rules('txtnote', 'Note', 'required');
	$this->form_validation->set_rules('txtnote', 'cover', 'required');
	$this->form_validation->set_rules('txtnote', 'catalog', 'required');
	$this->form_validation->set_rules('txtnote', 'draft', 'required');
		
		if ($this->form_validation->run() == false) {
            //GAGAL
            $data = array(
			  'title' => 'Reschedule',
			  'data_jb' => $this->Model_disposisi->get_all(),
			  'isi' => 'backend/disposisi/tambah_surat'
			);
			$this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
	
  }

  public function hapus($idj)
  {
    $id['id_disposisi'] = $this->uri->segment(3);
    $this->Model_disposisi->hapus($id);
    redirect('disposisiview');
  }
  
  public function disposisikan($idjb)
	{
        $idjb = $this->uri->segment(3);
		
		$this->form_validation->set_rules('txtkodesurat', 'Kode Surat', 'required');
        $this->form_validation->set_rules('txtperihal', 'Perihal', 'required');
		$this->form_validation->set_rules('txtasal', 'Asal', 'required');
        $this->form_validation->set_rules('txtunit', 'Unit', 'required');
		$this->form_validation->set_rules('txttgl', 'Tanggal', 'required');
        $this->form_validation->set_rules('txtwaktu', 'Waktu', 'required');
		$this->form_validation->set_rules('txtlokasi', 'Lokasi', 'required');
		$this->form_validation->set_rules('txtnote', 'Note', 'required');
		$this->form_validation->set_rules('txtnote', 'cover', 'required');
		$this->form_validation->set_rules('txtnote', 'catalog', 'required');
		$this->form_validation->set_rules('txtnote', 'draft', 'required');

        if ($this->form_validation->run() == false) {
            //GAGAL
            $data = array(
			  'title'     => 'Disposisi',
			  'data_surat_opd' => $this->Model_surat_opd->get_all(),
			  'data_jb' => $this->Model_disposisi->protokoler_disposisi($idjb),
			  'jns_as3' => $this->Model_disposisi->get_as3(),
			  'jns_pejabat' => $this->Model_disposisi->get_pejabat(),
			  'isi' => 'backend/disposisi/disposisi'
			);
			$this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
    }
  
  public function upstatus($idjb)
  {
	  $idjb = $this->uri->segment(3);	  
		$data = array(
		'id_login' => $this->session->userdata("id"),
		'tujuan' => $this->session->userdata("id_pengguna")
		);
		$this->db->where('id_surat_opd', $idjb);
		$this->db->update('protokoler', $data);
	
	$data = array(
	  'id_login' => $this->session->userdata("id"),
	  'disposisikan' => $this->session->userdata("id_pengguna")
    );
	$this->Model_disposisi->simpan($data);
	
	  $update = $this->db->query("UPDATE surat_opd SET status= '4', keterangan= 'Hadir' WHERE surat_opd.id_surat_opd = '$idjb'");
	  $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Simpan</div>');
	  redirect('disposisiview');
  }
  
  public function simpan()
  {
	$id = $this->input->post("TxtIDsuratopd");
    $data = array(
	  'id_login' => $this->session->userdata("id"),
	  'id_surat_opd' => htmlspecialchars($this->input->post("TxtIDsuratopd", true)),
	  'tujuan' => htmlspecialchars($this->input->post("txtdisposisi", true))
    );
	$this->db->where('id_surat_opd', $id);
	$this->db->update('protokoler', $data);
		
	
	$data = array(
	  'id_login' => $this->session->userdata("id"),
      'surat_opd' => htmlspecialchars($this->input->post("TxtIDsuratopd", true)),
	  'disposisikan' => htmlspecialchars($this->input->post("txtdisposisi", true)),
	  'note_disposisi' => htmlspecialchars($this->input->post("txtnote", true))
    );
	$update = $this->db->query("UPDATE surat_opd SET status= '2', keterangan= 'Disposisi' WHERE surat_opd.id_surat_opd = '$id'");
	
    $this->Model_disposisi->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Simpan</div>');

    redirect('disposisiview');
  }
	
  public function edit_jadwal($idjb)
	{
        $idjb = $this->uri->segment(3);
		
		$this->form_validation->set_rules('txtkodesurat', 'Kode Surat', 'required');
        $this->form_validation->set_rules('txtperihal', 'Perihal', 'required');
		$this->form_validation->set_rules('txtasal', 'Asal', 'required');
        $this->form_validation->set_rules('txtunit', 'Unit', 'required');
		$this->form_validation->set_rules('txttgl', 'Tanggal', 'required');
        $this->form_validation->set_rules('txtwaktu', 'Waktu', 'required');
		$this->form_validation->set_rules('txtlokasi', 'Lokasi', 'required');
		$this->form_validation->set_rules('txtnote', 'Note', 'required');
		$this->form_validation->set_rules('txtnote', 'cover', 'required');
		$this->form_validation->set_rules('txtnote', 'catalog', 'required');
		$this->form_validation->set_rules('txtnote', 'draft', 'required');

        if ($this->form_validation->run() == false) {
            //GAGAL
            $data = array(
			  'title'     => 'Reschedule',
			  'data_surat_opd' => $this->Model_surat_opd->get_all(),
			  'data_jb' => $this->Model_disposisi->surat_opd($idjb),
			  'jns_pejabat' => $this->Model_disposisi->get_pejabat(),
			  'isi' => 'backend/disposisi/perubahan_jadwal'
			);
			$this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
    }

  public function update()
  {
    $id['id_surat_opd'] = $this->input->post("TxtIDsuratopd");
	
    $data = array(
		'kode_surat' => htmlspecialchars($this->input->post("txtkodesurat", true)),
		'perihal' => htmlspecialchars($this->input->post("txtperihal", true)),
		'asal_surat' => htmlspecialchars($this->input->post("txtasal", true)),
		'unit_kerja' => htmlspecialchars($this->input->post("txtunit", true)),
		'tgl_kegiatan' => htmlspecialchars($this->input->post("txttgl", true)),
		'waktu' => htmlspecialchars($this->input->post("txtwaktu", true)),
		'lokasi' => htmlspecialchars($this->input->post("txtlokasi", true)),
		'note_opd' => htmlspecialchars($this->input->post("txtnote", true)),
		'status' => '3',
		'keterangan' => 'Reschedule'
        );

    $this->Model_surat_opd->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Update</div>');

    redirect('disposisiview');
  }

} // END OF class kecamatan
