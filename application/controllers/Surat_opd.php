<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_opd extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Model_surat_opd');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {
    $this->load->view('index');
  }
  
  public function viewPdf($nomor)
  {
	if (isset($_POST['view'])) {
	  header("content-type: application/pdf");
	  readfile('./berkas_lapiran/undangan/' . $nomor . '.pdf');
	}
  }
	
  public function tampil()
  {
    $data = array(
      'title' => '',
      'data_surat_opd' => $this->Model_surat_opd->get_all(),
	  'jns_pejabat' => $this->Model_surat_opd->get_pejabat(),
      'isi' => 'backend/surat_opd/data_tampil'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
	$data = array(
	  'title' => 'Input Surat',
	  'data_jb' => $this->Model_surat_opd->get_all(),
	  'jns_pejabat' => $this->Model_surat_opd->get_pejabat(),
	  'isi' => 'backend/surat_opd/tambah_surat'
	);
			
	$this->form_validation->set_rules('txtkodesurat', 'Kode Surat', 'required');
	$this->form_validation->set_rules('txtperihal', 'Perihal', 'required');
	$this->form_validation->set_rules('txtasal', 'Asal', 'required');
	$this->form_validation->set_rules('txtunit', 'Unit', 'required');
	$this->form_validation->set_rules('txttgl', 'Tanggal', 'required');
	$this->form_validation->set_rules('txtwaktu', 'Waktu', 'required');
	$this->form_validation->set_rules('txtlokasi', 'Lokasi', 'required');
	$this->form_validation->set_rules('txtpejabat', 'Dikemukakan Kepada', 'required');
	$this->form_validation->set_rules('txtnote', 'Note', 'required');
	$this->form_validation->set_rules('cover', 'cover', 'required');
	$this->form_validation->set_rules('catalog', 'catalog', 'required');
	$this->form_validation->set_rules('draft', 'draft', 'required');
		
		if ($this->form_validation->run() == false) {
            //GAGAL
            
			$this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
	
  }

  public function simpan()
  {
    $kodeqr = $this->Model_surat_opd->CreateCode();
	echo $kodeqr;
	
	$data = array(
	  'batch' => htmlspecialchars($kodeqr),
      'kode_surat' => htmlspecialchars($this->input->post("txtkodesurat", true)),
	  'perihal' => htmlspecialchars($this->input->post("txtperihal", true)),
	  'asal_surat' => htmlspecialchars($this->input->post("txtasal", true)),
	  'unit_kerja' => htmlspecialchars($this->input->post("txtunit", true)),
	  'tgl_kegiatan' => htmlspecialchars($this->input->post("txttgl", true)),
	  'waktu' => htmlspecialchars($this->input->post("txtwaktu", true)),
	  'lokasi' => htmlspecialchars($this->input->post("txtlokasi", true)),
	  'pejabat' => htmlspecialchars($this->input->post("txtpejabat", true)),
	  'note_opd' => htmlspecialchars($this->input->post("txtnote", true)),
	  'keterangan' => htmlspecialchars("Dalam Proses"),
      'id_login' => $this->session->userdata("id"),
	  'id_opd' => $this->session->userdata("id_pengguna")
    );
	
	// Cover upload
    $config = array();
	$config['file_name']		= $kodeqr;
    $config['upload_path'] 		= './berkas_lapiran/undangan/';
    $config['allowed_types'] 	= 'pdf';
	
    $this->load->library('upload', $config, 'coverupload'); // Create custom object for cover upload
    $this->coverupload->initialize($config);
    $upload_cover = $this->coverupload->do_upload('cover');
	// END Cover upload
    
	// Catalog upload
    $config = array();
	$config['file_name']		= $kodeqr;
    $config['upload_path'] = './berkas_lapiran/surat/';
    $config['allowed_types'] = 'pdf';

    $this->load->library('upload', $config, 'catalogupload');  // Create custom object for catalog upload
    $this->catalogupload->initialize($config);
    $upload_catalog = $this->catalogupload->do_upload('catalog');
	// END Catalog upload
	
	// Draft upload
    $config = array();
	$config['file_name']		= $kodeqr;
    $config['upload_path'] = './berkas_lapiran/sambutan/';
    $config['allowed_types'] = 'pdf';

    $this->load->library('upload', $config, 'draftupload'); // Create custom object for draft upload
    $this->draftupload->initialize($config);
    $upload_draft = $this->draftupload->do_upload('draft');
	// END Draft upload

	
	if (!$upload_cover && $upload_catalog && $upload_draft) {
			$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal! Hanya Menerima Type File PDF.</div>');
			redirect('tambahpemohon');
		} else{
			//$data = $this->catalogupload->data();
			//return $data['file_name'];
			$cover_data = $this->coverupload->data();
			//$filename = $cover_data['file_name']; 
            $filename = $kodeqr; 
			$data['daf_undangan'] = htmlspecialchars($kodeqr);
			print_r($cover_data);
			
			$catlog_data = $this->catalogupload->data();          
			//$filename = $catlog_data['file_name']; 
            $filename = $kodeqr; 
			$data['daf_surat'] = htmlspecialchars($kodeqr);
			print_r($catlog_data);
			
			$draft_data = $this->draftupload->data();
			//$filename = $draft_data['file_name']; 
            $filename = $kodeqr; 
			$data['daf_sambutan'] = htmlspecialchars($kodeqr);
			print_r($draft_data);
		
	
    $this->Model_surat_opd->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Simpan</div>');

    redirect('opdentry');
	}
  
  }

  public function hapus($idj)
  {
    $id['id_surat_opd'] = $this->uri->segment(3);
    $this->Model_surat_opd->hapus($id);
    redirect('opdview');
  }
  
  public function edit($idjb)
	{
        $idjb = $this->uri->segment(3);
		
		$data = array(
			  'title'     => 'Update Surat',
			  'data_jb' => $this->Model_surat_opd->edit($idjb),
			  'jns_pejabat' => $this->Model_surat_opd->get_pejabat(),
			  'isi' => 'backend/surat_opd/perubahan_surat'
		);
		
		$this->form_validation->set_rules('txtkodesurat', 'Kode Surat', 'required');
        $this->form_validation->set_rules('txtperihal', 'Perihal', 'required');
		$this->form_validation->set_rules('txtasal', 'Asal', 'required');
        $this->form_validation->set_rules('txtunit', 'Unit', 'required');
		$this->form_validation->set_rules('txttgl', 'Tanggal', 'required');
        $this->form_validation->set_rules('txtwaktu', 'Waktu', 'required');
		$this->form_validation->set_rules('txtlokasi', 'Lokasi', 'required');
        $this->form_validation->set_rules('txtpejabat', 'Dikemukakan Kepada', 'required');
		$this->form_validation->set_rules('txtnote', 'Note', 'required');
		$this->form_validation->set_rules('cover', 'cover', 'required');
		$this->form_validation->set_rules('catalog', 'catalog', 'required');
		$this->form_validation->set_rules('draft', 'draft', 'required');

        if ($this->form_validation->run() == false) {
            //GAGAL
            
			$this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->update();
        }
    }

  public function update()
  {
    $id['id_surat_opd'] = $this->input->post("TxtIDsuratopd");
	
    $data = array(
		'kode_surat' => htmlspecialchars($this->input->post("txtkodesurat", true)),
		'perihal' => htmlspecialchars($this->input->post("txtperihal", true)),
		'asal_surat' => htmlspecialchars($this->input->post("txtasal", true)),
		'unit_kerja' => htmlspecialchars($this->input->post("txtunit", true)),
		'tgl_kegiatan' => htmlspecialchars($this->input->post("txttgl", true)),
		'waktu' => htmlspecialchars($this->input->post("txtwaktu", true)),
		'lokasi' => htmlspecialchars($this->input->post("txtlokasi", true)),
		'pejabat' => htmlspecialchars($this->input->post("txtpejabat", true)),
		'note_opd' => htmlspecialchars($this->input->post("txtnote", true))
        );

    $this->Model_surat_opd->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Update</div>');

    redirect('opdview');
  }

} // END OF class kecamatan
