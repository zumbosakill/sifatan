<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_userz extends CI_Model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_user')
            ->join('tbl_adm','tbl_adm.id_pengguna=tbl_user.id_pengguna')
            ->get();
        return $query->result();
    }

	
    function cek_login($table, $where)
    {
        return $this->db->get_where($table, $where)->row_array();
    }

    public function getp($username, $password)
    {
        $query = $this->db->where("username", $username)
            ->where("password", $password)
            ->get("tbl_user");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_user", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function editx($idjx)
	{
		$query = $this->db->where("id_pengguna", $idjx)
		  ->get("tbl_user");  
		if ($query) {
		  return $query->row();
		} else {
		  return false;
		}
	}
	
	public function edit($idjb)
	{
		$query = $this->db->where("id_pengguna", $idjb)
		  ->get("tbl_adm");  
		if ($query) {
		  return $query->row();
		} else {
		  return false;
		}
	}

    public function update($data, $id)
    {
        $query = $this->db->where("id_pengguna", $id)
            ->update("tbl_user", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	public function updatex($data, $id)
    {
        $query = $this->db->where("id_pengguna", $id)
            ->update("tbl_adm", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapusu($idu)
    {
        $query = $this->db->delete("tbl_user", $idu);
		$query = $this->db->delete("tbl_adm", $idu);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
