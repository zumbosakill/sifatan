<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_disposisi extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('disposisi')
      ->order_by('id_disposisi', 'ASC')
      ->get();
    return $query->result();
  }
  
  public function get_as3()
  {
    $query = $this->db->query("SELECT * FROM pejabat WHERE ditujukan='1' ");
    return $query->result();
  }
  
  public function get_protokoler()
  {
    $query = $this->db->select("
	protokoler.id_surat_opd AS idsuratopd,
	protokoler.tujuan,
	protokoler.note_protokoler,
	surat_opd.kode_surat,
	surat_opd.perihal,
	surat_opd.asal_surat,
	surat_opd.unit_kerja,
	surat_opd.tgl_kegiatan,
	surat_opd.waktu,
	surat_opd.lokasi,
	surat_opd.pejabat,
	surat_opd.note_opd,
	surat_opd.daf_undangan,
	surat_opd.daf_surat,
	surat_opd.daf_sambutan,
	surat_opd.status,
	surat_opd.keterangan
	")
      ->from('surat_opd')
	  ->join('protokoler','surat_opd.id_surat_opd=protokoler.id_surat_opd')
      ->order_by('tgl_kegiatan', 'DESC')
      ->get();
    return $query->result();
  }
  
  public function get_pejabat()
  {
    $query = $this->db->select("*")
      ->from('pejabat')
      ->order_by('id_pejabat', 'ASC')
      ->get();
    return $query->result();
  }

  public function protokoler_disposisi($idjb)
  {
    $query = $this->db->query("SELECT
	protokoler.tujuan,
	protokoler.note_protokoler,
	surat_opd.id_surat_opd,
	surat_opd.kode_surat,
	surat_opd.perihal,
	surat_opd.asal_surat,
	surat_opd.unit_kerja,
	surat_opd.tgl_kegiatan,
	surat_opd.waktu,
	surat_opd.lokasi,
	surat_opd.pejabat
	FROM surat_opd
	JOIN protokoler ON surat_opd.id_surat_opd=protokoler.id_surat_opd
	WHERE surat_opd.id_surat_opd ='$idjb'");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }
  
  public function simpan($data)
  {
    $query = $this->db->insert("disposisi", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($id)
  {
    $query = $this->db->delete("disposisi", $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
  public function surat_opd($idjb)
  {
    $query = $this->db->where("id_surat_opd", $idjb)
      ->get("surat_opd");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }
  
  
  
  public function edit($idjb)
  {
    $query = $this->db->where("id_disposisi", $idjb)
      ->get("disposisi");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("disposisi", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
} // END OF class Model_disposisi
