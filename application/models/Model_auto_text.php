<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_auto_text extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('auto_text')
      ->order_by('id_auto_text', 'ASC')
      ->get();
    return $query->result();
  }

	
  public function simpan($data)
  {
    $query = $this->db->insert("auto_text", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($id)
  {
    $query = $this->db->delete("auto_text", $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
  
  public function edit($idjb)
  {
    $query = $this->db->where("id_auto_text", $idjb)
      ->get("auto_text");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("auto_text", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
  
} // END OF class Model_auto_text
